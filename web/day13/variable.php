<?php 
global $students, $genderArray, $departments;

$genderArray = array("1" => "Nam", "0" => "Nữ");

$departments = array("None" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học dữ liệu");

$students = array(
        "Trần Văn A"=>"Khoa học máy tính",
        "Nguyễn Thị B"=>"Khoa học máy tính",
        "Hoàng Văn C"=>"Khoa học dữ liệu",
        "Đinh Thị D"=>"Khoa học dữ liệu",
);
?>
