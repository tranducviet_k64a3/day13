document.addEventListener("DOMContentLoaded", () => {
    /**
     * Đóng mở dropdown bằng button
     * CreatedBy: Viet2707 - 23/09/2022
     */
    var dropdown = document.querySelector(".dropdown")
    document.querySelector("#button-dropdown").addEventListener("click", () => {
        dropdown.classList.toggle("hide")
        dropdown.focus()
    })

    /**
     * Chọn option
     * CreatedBy: Viet2707 - 23/09/2022
     */
    var options = document.querySelectorAll(".dropdown li")
    var input = document.querySelector("#select-input")
    options.forEach(option => {
        option.addEventListener("click", () => {
            var value = option.innerHTML;
            input.setAttribute("value", value)
            input.setAttribute("option", option.getAttribute("value"))
            dropdown.classList.add("hide")
        })
    })

});