<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
</head>
<body>
    <?php
        /**
         * Khai báo các biến cần thiết
         * CreatedBy: Viet2707 - 10/11/2022
         */
        session_start();
        include ("../variable.php");
        date_default_timezone_set("Asia/Ho_Chi_Minh");
        $target_dir = "../upload/";
        $regrex = "/^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/";
        $name = $date = $gender = $department = $address = '';
        $errors = array('name' => '', 'gender' => '', 'department' => '', 'date' => '', 'image' => '');
       
        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            $valid = TRUE;
            
            /**
             * Check tên
             * CreatedBy: Viet2707 - 10/11/2022
             * */ 

            if (empty($_POST['name'])) { 
                $valid = FALSE;
                $errors['name'] = "<h6 style='color:red;'>Hãy nhập tên</h6>";
            } else {
                $name = $_POST['name'];
            }

            /**
             * Check giới tính
             * CreatedBy: Viet2707 - 10/11/2022
             * */ 
            if (!isset($_POST['gender'])) {
                $valid = FALSE;
                $errors['gender'] = "<h6 style='color:red;'>Hãy chọn giới tính</h6>";
            } else {
                $gender = $_POST['gender'];
            } 
            
            /**
             * Check khoa
             * CreatedBy: Viet2707 - 10/11/2022
             * */ 
            if (empty($_POST['department'])) {
                $valid = FALSE;
                $errors['department'] = "<h6 style='color:red;'>Hãy chọn phân khoa</h6>";
            } else {
                $department = $_POST['department'];
                $_POST['department'] = array_keys( $departments, $_POST['department'])[0];
            }

            /**
             * Check ngày sinh
             * CreatedBy: Viet2707 - 10/11/2022
             * */ 
            if (empty($_POST['date'])) {
                $valid = FALSE;
                $errors['date'] = "<h6 style='color:red;'>Hãy nhập ngày sinh</h6>";
            } elseif (!preg_match($regrex, $_POST['date'])) {
                $errors['date'] = "<h6 style='color:red;'>Hãy nhập ngày sinh đúng định dạng</h6>";
            } else {
                $date = $_POST['date'];
            }
            
            /**
             * Lấy ra địa chỉ
             * CreatedBy: Viet2707 - 10/11/2022
             * */ 
            $address = $_POST['address'];

            /**
             * Check ảnh đúng định dạng chưa
             * Nếu có thì đưa vào thư mục upload
             * Nếu không thì đưa ra cảnh báo lỗi
             * CreatedBy: Viet2707 - 10/11/2022
             * */ 
            if ($_FILES["imageFile"]["size"] > 0 ) {
                $finfo = finfo_open(FILEINFO_MIME_TYPE);
                $mimetype = finfo_file($finfo, $_FILES["imageFile"]["tmp_name"]);
                if($mimetype == 'image/jpg' || $mimetype == 'image/jpeg' || $mimetype == 'image/gif' || $mimetype == 'image/png') {
                    // Kiểm tra xem đã tồn tại thư mục upload chưa
                    if (!is_dir($target_dir)) {
                        mkdir($target_dir, 0777, true);
                    }

                    // Đổi tên ảnh
                    $temp = explode(".", $_FILES["imageFile"]["name"]);
                    $filename = $temp[0] . "_" . date('YmdHis') . "." . $temp[1];

                    // Lấy ra đường link ảnh
                    $target_file = $target_dir . basename($filename);

                    // Di chuyển ảnh theo đường dẫn đã tạo
                    if (move_uploaded_file($_FILES["imageFile"]["tmp_name"], $target_file)) {
                        $_POST["imageFile"] = "../upload/".$filename;
                    } 
                } else {
                    $valid = FALSE;
                    $errors['image'] = "<h6 style='color:red;'>Hãy thêm đúng định dạng là file ảnh</h6>";
                }
            }
 
            if ($valid) {
                $_SESSION["data"] = $_POST;
                header("location: ./submit.php");
            }

        }
    ?>

    <!-- ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ -->
    <div class="center">
        <div class="container">            
            <form action="" method="POST" enctype="multipart/form-data">
                <div class="wrapper">
                    <div>
                        <?php
                            echo $errors['name'];
                            echo $errors['gender'];
                            echo $errors['department'];
                            echo $errors['date'];
                            echo $errors['image'];
                        ?>  
                    </div>
                    <div class="label-row name">
                        <button class="compulsory">Họ và tên</button>
                        <input type="text" name="name" value="<?php echo $name ?>">
                    </div>
                    <div class="label-row gender">
                        <button class="compulsory">Giới tính</button>
                        <?php 
                            for ($i = 0; $i < count($genderArray); $i++) {
                                echo "<div class='gender-option'>
                                    <input type='radio' name='gender' id=gender-$i value=$i ";
                                if ($i == $gender) {
                                    echo "checked";
                                } else {
                                    echo "";
                                }
                                echo ">";
                                echo "<label>$genderArray[$i]</label>";
                                echo "</div>";
                            }
                        ?>
                    </div>
                    <div class="label-row department">
                        <button class="compulsory">Phân khoa</button>
                        <div class="select-box">
                            <input id="select-input" readonly type="text" name="department" value="<?php echo $department ?>">
                            <div class="arrow-down" id="button-dropdown"></div>
                            <ul class="dropdown hide" tabindex="-1">
                                <?php
                                    foreach ($departments as $department => $department_value) {
                                        echo "<li value=$department>$department_value</li>";                        
                                    }
                                ?>  
                            </ul>
                        </div>
                    </div>
                    <div class="label-row date">
                        <button class="compulsory">Ngày sinh</button>
                        <div class="relative">
                            <div class="input-group date" id="datepicker">
                                <input type="text" placeholder="dd/mm/yyyy" class="form-control" name="date" value="<?php echo $date ?>">
                                <span class="input-group-append">
                                    <span class="input-group-text bg-white">
                                        <i class="fa fa-calendar"></i>
                                    </span>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="label-row address">
                        <button>Địa chỉ</button>
                        <input type="text" name="address" value="<?php echo $address ?>">
                    </div>
                    <div class="label-row image">
                        <button>Hình ảnh</button>
                        <input type="file" name="imageFile" id="imageFile"/>
                    </div>
                    <div class="submit">
                        <input type="submit" value="Đăng ký" name="submit" >
                    </div>
                </div>
            </form>
        </div>
    </div>
</body>
<style>
<?php 
    include '../styles/global.css'; 
    include '../styles/register.css'; 
    ?>
</style>
<script src="../js/dropdown.js"></script>
<script type="text/javascript">
    $(function() {
        $('#datepicker').datepicker({
                format: "dd/mm/yyyy"
        });
    });
</script>
</html>