<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Student</title>
    <link rel="stylesheet" href="../styles/global.css">
    <link rel="stylesheet" href="../styles/register.css">
</head>
<body>
    <?php
        session_start();
        include ('../connection.php');
        $name = $_SESSION['data']['name'];
        $gender = $_SESSION['data']['gender'];
        $fac  = $_SESSION['data']['department'];
        $birthday = date("Y-m-d", strtotime(implode("-", explode("/", $_SESSION['data']['date']))));
        $address = $_SESSION['data']['address'];
        $avartar = $_SESSION['data']['imageFile'];
        $sql = "INSERT INTO `student` (`name`, `gender`, `faculty`, `birthday`, `address`, `avartar`) VALUES ('$name', '$gender', '$fac', '$birthday', '$address', '$avartar')";
        $connection -> exec($sql);
    ?>

    <div class="center">
        <div class="container">
            <div class="wrapper wrapper--center">
                <p>Bạn đã đăng ký thành công sinh viên</p>
                <a href="../index.php">Quay lại danh sách sinh viên</a>
            </div>
        </div>
    </div>
</body>
</style>
</html>